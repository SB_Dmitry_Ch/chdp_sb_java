package src.thirdweekpractice;
//first commit
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {

        String inputString = new Scanner(System.in).nextLine();

        System.out.println("Initial value: " + inputString);
        System.out.println("Updated value: " + inputString.replaceAll("([a-z])([A-Z]+)","$1_$2").toLowerCase());



    }
}
