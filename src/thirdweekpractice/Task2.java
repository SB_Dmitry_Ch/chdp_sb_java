package src.thirdweekpractice;

import java.util.Scanner;

/*
Проверить номер карты и PIN код

*/
public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String cardNumber = input.nextLine();
        String code = input.nextLine();

        System.out.println("Card number is valid: " + cardNumber.matches("([0-9]{4} ){3}[0-9]{4}"));
        //System.out.println("Card number is valid: " + cardNumber.matches("(\\d{4} ){3}\\d{4}"));
        System.out.println("PIN is valid: " + code.matches("[0-9]{4}"));

    }
}
