//package src.main.java;//

import java.util.Scanner;

public class Dz1_1 {
    public static void main(String[] args) {
        double vol;
        Scanner scan = new Scanner(System.in);
        int r = scan.nextInt();
        vol = 4.0 / 3.0 * Math.PI * Math.pow(r, 3);

        System.out.println(vol);
    }
}
