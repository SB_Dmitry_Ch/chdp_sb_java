package src.main.java;//package src.main.java;

import java.util.Scanner;

public class Dz1_5 {
    static final double INCH_IN_CM = 2.54;

    public static void main(String[] args) {

        double cm;
        Scanner scan = new Scanner(System.in);

        cm = INCH_IN_CM * scan.nextFloat();

        System.out.println(cm);
    }
}
