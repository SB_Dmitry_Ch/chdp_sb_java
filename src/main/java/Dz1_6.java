package src.main.java;//package src.main.java;

import java.util.Scanner;

public class Dz1_6 {
    static final double MILES_IN_KM = 1.60934;

    public static void main(String[] args) {

        double count;
        Scanner scan = new Scanner(System.in);

        count = scan.nextFloat() / MILES_IN_KM;

        System.out.println(count);
    }
}
