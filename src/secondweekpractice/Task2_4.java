package src.secondweekpractice;

import java.util.Scanner;

public class Task2_4 {
    public static final int  VIP_PRICE = 125;
    public static final int  PREMIUM_PRICE = 110;
    public static final int  STANDARD_PRICE = 100;

    public static void main(String[] args) {
       // int scanner;

        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
//        if (i == 1) {
//            System.out.println("VIP Price: " + VIP_PRICE);
//        }
//        if (i == 2) {
//            System.out.println("Premium Price: " + PREMIUM_PRICE);
//        }
//        if (i == 3) {
//            System.out.println("Standard Price: " + STANDARD_PRICE);
//        }

//  новая версия switch (через лямбды)



// стандартная версия switch (поддерживается всеми версиями Java)
        switch (i) {
            case 1:
                System.out.println("VIP Price: " + VIP_PRICE);
                break;
            case 2:
                System.out.println("Premium Price: " + PREMIUM_PRICE);
                break;
            case 3:
                System.out.println("Standard Price: " + STANDARD_PRICE);
                break;
            default:
                System.out.println("введите корректный номер");

        }

    }

}
